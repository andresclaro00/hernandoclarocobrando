const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
  user: String,
  pass: String
}, {timestamps: true, collection: 'users' });

module.exports = User = mongoose.model('user', UserSchema);