const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
  name: String,
  cels: Array
}, { timestamps: true, collection: 'data' });

module.exports = Data = mongoose.model('data', UserSchema);