const express = require("express")
const app = express()
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const jwt = require('jwt-simple')
const multer  = require('multer')
const upload = multer()
const excelToJson = require('convert-excel-to-json');

//Req props
const User = require('./src/modelos/Users')
const Data = require('./src/modelos/Data')
const secret = "CoBrAnDo"

// Configuraciones
// ---Express
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())
app.use(express.static(__dirname + '/public'));

// ---Mongoose
const options = {useUnifiedTopology: true, useNewUrlParser: true}
mongoose.connect('mongodb://localhost:27017/cobrandodb', options)
    .then(
        () => console.log("Conectado a mongodb"),
        err => console.log("Error")
    );

app.get("/", function(req, res) {
  res.sendFile(process.cwd() + '/public/index.html');
});

app.get("/pag", function(req, res) {
    res.sendFile(process.cwd() + '/public/pag.html');
  });

app.post('/login', (req, res) => {
    User.findOne({user: req.body.user})
    .then((data) => {
        if(data != null && req.body.pass == data.pass){
            let tok = jwt.encode(data._id, secret)
            return res.status(200).send({token: tok})
        }else{
            return res.send("err")
        }
    }).catch((e) => {
        return res.send("err")
    })
})

app.get('/actualData', guardd, (req, res) => {
    let arr = []
    Data.find({}).then((data) => {
        data.forEach((data1) => {
            for(o=0; o<data1.cels.length; o++){
                if(data1.cels[o].data == undefined){
                    data1.cels[o].data = ""
                }
            }
            arr.push({name: data1.name, cels: data1.cels})
        });
        res.send({data:arr})        
    })
})

app.post('/putData', upload.single('data'), async (req, res) => {
    
    let result = excelToJson({source: req.file.buffer})

    let titles = result[Object.keys(result)[0]][0]
    let datac = result[Object.keys(result)[0]]
    let cols = Object.keys(datac[0])

    Object.keys(titles).forEach((key, ind) => {
        let columnName = titles[key]
        Data.findOne({name: columnName})
        .then((data) => {
            if(data == null){
                let arr = []
                for(i=1; i < Object.keys(datac).length ; i++){
                    arr[i-1] = {data: datac[i][cols[ind]]}
                }
                Data.create({name: columnName, cels: arr})
                .then((err,data) => {
                    if(!err) console.log("Guardado")
                })
            }
        })
    }) 

    let html = "Archivo guardado correctamente</br></br>"
    html += "<a href='http://localhost:3000/pag'><button>Volver</button></a>"

    return res.send(html)
})

async function guardd(req, res, next){
    let head = req.headers
    if(head.authorization.split(' ')[1] == "null"){
        return res.send({msg:"err", user: ""})
    }
    let tk = head.authorization.split(' ')[1]
    let payload = jwt.decode(tk, secret)
    let user = await User.findById(payload)
    if(!user) return res.send({msg:"err", user: ""})    
    next()
}

app.listen(3000, () => {
    console.log("corriendo")
})