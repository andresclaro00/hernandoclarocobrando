window.onload = function() {
    let size = 0, sizeA, arr = [], arrN = []
    let token = localStorage.getItem('token')
    const url = "http://localhost:3000/actualData"
    const xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.setRequestHeader("Authorization", "bearer " + token);
    xhr.addEventListener('load', function() {
      let responseObject = JSON.parse(this.response);
      if(responseObject.msg == "err"){
        window.location = "/";
      }
      Object.keys(responseObject.data).forEach((datan) => {
        sizeA = Object.keys(responseObject.data[datan].cels).length
        if(sizeA > size){
            size = sizeA
        }
      })
      let arraySize = Object.keys(responseObject.data).length
      for(i=0; i<arraySize; i++){
        arrN[i] = responseObject.data[i].name
      }
      arr.push(arrN)
      for(i=0; i<size; i++){
        let arr1 = []
          for(j=0; j<arraySize; j++){
            let d = responseObject.data[j].cels[i]
            let d2 = ""
            if(d){
                d2 = d.data
            }
            arr1.push(d2)
          } 
          arr.push(arr1)      
      }
      let html = "<table>"
      for(i=0; i<arr.length; i++){
          html += "<tr>"
          arr[i].forEach((data) => {
            html += "<td>" + data + "</td>";
          })
          html += "</tr>"
      }
      html += "</table>";
      document.getElementById("table").innerHTML = html;
    });
  
    xhr.send(null);
  }

  function salir(){
      console.log("Salir")
      localStorage.removeItem('token');
      window.location = "/";
  }