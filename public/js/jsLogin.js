function getToken() {
    const loginUrl = "http://localhost:3000/login"
    const xhr = new XMLHttpRequest();
    let userElement = document.getElementById('username');
    let passwordElement = document.getElementById('password');
    let user = userElement.value;
    let password = passwordElement.value;
  
    if(user != "" && password != ""){
        xhr.open('POST', loginUrl, true);
        xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
        xhr.addEventListener('load', function() {
            if(this.response != "err"){
                let responseObject = JSON.parse(this.response);
                if (responseObject.token) {
                    localStorage.setItem('token', responseObject.token);
                    window.location = "/pag";
                } else {
                    tokenElement.innerHTML = "No hay token";
                }
            }else{
                alert("Usuario o contraseña incorrecto")
            }
        });
        let sendObject = JSON.stringify({user: user, pass: password}); 
        xhr.send(sendObject);
    }else{
        alert("Use todos los campos")
    }
  }